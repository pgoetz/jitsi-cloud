FROM ubuntu:20.04

RUN apt-get update

# Enable SSH key generation
RUN apt-get install -y openssh-client

# Install Python and Ansible modules
RUN apt-get install -y python3 python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install boto boto3 botocore ansible

# Install Azure modules
RUN pip3 install -U -I 'ansible[azure]'

# Use Python 3
RUN ln -fs /usr/bin/python3 /usr/bin/python
